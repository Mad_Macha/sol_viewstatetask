﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_Poco_ViewState_label
{
    public class UserEntity
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LName { get; set; }
    }
}