﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_Poco_ViewState_label
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        int counter = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack == false)
            {
                txtName.Text = counter.ToString();
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            // check view state value null or not
            if (this.ViewState["ClickCounter"] != null)
            {
                // Set fresh Counter value in ViewState
                this.ViewState["ClickCounter"] = ((int)ViewState["ClickCounter"]) + 1;

                counter = (int)ViewState["ClickCounter"];
            }

            // Bind in TextBox.
            txtName.Text = counter.ToString();

            // store Value in View State
            this.ViewState["ClickCounter"] = counter;
        }
    }
}