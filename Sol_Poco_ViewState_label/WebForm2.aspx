﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm2.aspx.cs" Inherits="Sol_Poco_ViewState_label.WebForm2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:TextBox ID="txtId" placeholder="ID" runat="server"></asp:TextBox><br/><br/>
    <asp:TextBox ID="txtName" placeholder="FirstName" runat="server"></asp:TextBox><br/><br/>
        <asp:TextBox ID="txtLname" placeholder="LastName" runat="server"></asp:TextBox><br/><br/>
            <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click"  /><br/><br/>
        <asp:Label ID="lblDisplay" runat="server" Text="Label">
            <%Response.Write(Server.HtmlEncode(entityObj?.ID.ToString())); %><br/>
            <%Response.Write(Server.HtmlEncode(entityObj?.FirstName.ToString())); %><br/>
            <%Response.Write(Server.HtmlEncode(entityObj?.LName.ToString())); %><br/>
        </asp:Label>

    </div>
        
    </form>
</body>
</html>
